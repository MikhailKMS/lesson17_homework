package ru.sber.jd.services;

import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import ru.sber.jd.entities.TransportNumberEntity;
import ru.sber.jd.repositories.TransportNumberRepository;

import java.util.Collections;
import java.util.Properties;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MainService implements CommandLineRunner {

    private final static String TOPIC = "jd-topic";
    private final static String BOOTSTRAP_SERVER = "localhost:9092";

    private final TransportNumberRepository transportNumberRepository;

    @Override
    public void run(String... args) throws Exception {

        Thread producerThread = new Thread(() -> {
            final Producer<String, String> producer = createProducer();
            while (true) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Random r = new Random();
                String dict = "авекмнорстух";

                String randomTransportNumber = "" + dict.charAt(r.nextInt(dict.length()));
                randomTransportNumber = randomTransportNumber + r.nextInt(10);
                randomTransportNumber = randomTransportNumber + r.nextInt(10);
                randomTransportNumber = randomTransportNumber + r.nextInt(10);
                randomTransportNumber = randomTransportNumber + dict.charAt(r.nextInt(dict.length()));
                randomTransportNumber = randomTransportNumber + dict.charAt(r.nextInt(dict.length()));

                final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, "1", randomTransportNumber);
                try {
                    RecordMetadata metadata = producer.send(record).get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        producerThread.start();



        Thread consumerThread = new Thread(() -> {
            final Consumer<String, String> consumer = createConsumer();
            while (true) {
                final ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);

                if (consumerRecords.count() == 0) {
                    continue;
                }
                consumerRecords.forEach(record -> {

                    TransportNumberEntity transportNumberEntity = new TransportNumberEntity();
                    transportNumberEntity.setTransportNumber(record.value());
                    transportNumberRepository.save(transportNumberEntity);

                    System.out.println(transportNumberRepository.findById((int) transportNumberRepository.count()));

                });
                consumer.commitAsync();
            }
        });
        consumerThread.start();

    }


    private static Producer<String, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer<>(props);
    }

    private static Consumer<String, String> createConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC));
        return consumer;
    }


}
