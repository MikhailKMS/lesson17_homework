package ru.sber.jd.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.jd.entities.TransportNumberEntity;

@Repository
public interface TransportNumberRepository extends JpaRepository<TransportNumberEntity, Integer> {
}
